require 'time'
require 'date'

class Time

  VALID_TIME_REGEX = /^(([0,1]?[0-9])|([2]?[0-3])):[0-5]?[0-9]:[0-5]?[0-9]$/
  BASE_TIME = Time.parse("00:00:00")

  def self.add(time_1, time_2)
    time_list = [time_1, time_2]
    return unless valid?(time_list)
    time = BASE_TIME + time_list.inject(0) { |total_time, time_string| total_time += Time.parse(time_string) - BASE_TIME }
    days = no_of_days(time, BASE_TIME)
    number_of_day_with_time(time, days)
  end

  protected
    def self.number_of_day_with_time(time, days)
      days > 0 ? "#{ days } day & " + show(time) : show(time)
    end

    def self.valid?(time_list)
      time_list.all? { |time_string| (time_string =~ VALID_TIME_REGEX) == 0 }
    end

    def self.show(time)
      time.strftime "%H:%M:%S"
    end

    def self.no_of_days(time_1, time_2)
      Date.parse(time_1.to_s).day - Date.parse(time_2.to_s).day
    end
end

